﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    public class Runner
    {
        public void Run()
        {
            Console.WriteLine("Hello World!");

            Calculator calculator = new Calculator();
            Console.WriteLine($"{calculator.Add(5, 7)}");
            Console.WriteLine($"{calculator.Substract(5, 7)}");
            Console.WriteLine($"{calculator.Multiplication(5, 7)}");

            Console.ReadKey();
        }
    }
}
