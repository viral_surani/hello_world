﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    public class Calculator
    {
        public int Add(int left, int right)
        {
            return (left + right);
        }

        public int Substract(int left, int right)
        {
            return (left - right);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public int Multiplication(int left, int right)
        {
            return (left * right);
        }

        public int Modulus(int left, int right)
        {
            return left % right;
        }
    }
}
